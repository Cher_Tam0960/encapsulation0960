/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kitti
 */
public class Triangle {
    private double b;
    private double h;
    
    public Triangle(double b, double h){
        this.b = b;
        this.h = h;
    }
    public double calAreatri(){
        double result = 0.5 * b * h;
        return result;
    }
    public void SetTri(double b, double h){
         if (b <= 0 || h <= 0) {
            System.out.println("Error : side must more tha zero!!");
            return; 
        }
        this.b = b;
        this.h = h;
     }
}
